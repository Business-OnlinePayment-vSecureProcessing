#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Business::OnlinePayment::vSecureProcessing' ) || print "Bail out!
";
}

diag( "Testing Business::OnlinePayment::vSecureProcessing $Business::OnlinePayment::vSecureProcessing::VERSION, Perl $], $^X" );
