
sub test_account {
    # fill all these fields in to test out transactions
    my %opts = (
        login    => '', #userid
        password => '', #gid
        platform => '',
        appid => ''
        #tid => '',
    );

    return %opts;
}

sub expiration_date {
    my($month, $year) = (localtime)[4,5];
    $month += 1;
    $year++;       # So we expire next year.
    $year %= 100;

    return sprintf("%02d/%02d", $month, $year);
}


1;

