#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw(strftime);
use Test::More;
use Business::OnlinePayment;
require "t/lib/test_account.pl";

my %opts = test_account('card');

if (!$opts{'login'} || !$opts{'password'}) {
  plan skip_all => "no test credentials provided; fill out t/lib/test_account.pl to test communication with the gateway.",
  1;
  exit(0);
}

plan tests => 1; #2;
my %content = (
    login          => delete $opts{'login'},
    password       => delete $opts{'password'},
    action         => 'Normal Authorization',
    description    => 'Business::OnlinePayment visa test',
    card_number    => '4111111111111112', # trigger failure
    cvv2           => '111',
    expiration     => expiration_date(),
    amount         => '24.42',
    name           => 'Murphy Law',
    email          => 'fake@acme.com',
    address        => '123 Anystreet',
    zip            => '84058',
);

my $tx = new Business::OnlinePayment( 'vSecureProcessing', %opts );

$tx->content( %content );

$tx->test_transaction(1);

$tx->submit;

is( $tx->is_success, 0, 'declined purchase')
  or diag('Test transaction should have failed, but succeeded');
#is( $tx->failure_status, 'nsf', 'failure status' )
#  or diag('Failure status reported as '.$tx->failure_status);

1;
