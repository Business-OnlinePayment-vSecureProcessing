#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw(strftime);
use Test::More;
use Business::OnlinePayment;
require "t/lib/test_account.pl";

my %opts = test_account('card');

if (!$opts{'login'} || !$opts{'password'}) {
  plan skip_all => "no test credentials provided; fill out t/lib/test_account.pl to test communication with the gateway.",
  1;
  exit(0);
}

plan tests => 2;

###
# Purchase
###
my %content = (
    login          => delete($opts{'login'}),
    password       => delete($opts{'password'}),
    action         => 'Normal Authorization',
    description    => 'Business::OnlinePayment visa test',
    card_number    => '4111111111111111',
    expiration     => expiration_date(),
    amount         => '24.42',
    name           => 'Murphy Law',
    email          => 'fake@acme.com',
    address        => '123 Anystreet',
    zip            => '84058',
);

my $tx = new Business::OnlinePayment( 'vSecureProcessing', %opts );

$tx->content( %content );

$tx->test_transaction(1);

$tx->submit;

is( $tx->is_success, 1, 'purchase' )
  or diag('Gateway error: '. $tx->error_message);

###
# Refund
###
my $auth = $tx->authorization;
$tx = new Business::OnlinePayment( 'vSecureProcessing', %opts );
$tx->content( %content,
              action => 'Credit',
              authorization => $auth );
$tx->test_transaction(1);

$tx->submit;

is( $tx->is_success, 1, 'refund' )
  or diag('Gateway error: '. $tx->error_message);

1;
